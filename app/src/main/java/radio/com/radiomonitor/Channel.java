package radio.com.radiomonitor;

import android.os.Parcel;
import android.os.Parcelable;
import android.widget.CheckBox;

/**
 * Created by triet.bui on 6/30/2016.
 */
public class Channel implements Parcelable{

    public String url;
    public String port;
    public String name;

    public int listener;
    public int unique;
    public int peak;
    public int offline;

    public Channel() {

    }

    @Override
    public boolean equals(Object o) {
        if(o == null)
            return false;

        if(!(o instanceof Channel))
            return false;

        Channel that = (Channel) o;

        return (url != null && url.equals(that.url)) &&
                (port != null && port.equals(that.url));
    }

    @Override
    public int hashCode() {
        int result = 17;
        result += 31 * result + (url == null ? 0 : url.hashCode());
        result += 31 * result + (port == null ? 0 : port.hashCode());

        return result;
    }

    protected Channel(Parcel in) {

        url = in.readString();
        port = in.readString();
        name = in.readString();
        listener = in.readInt();
        unique = in.readInt();
        peak = in.readInt();
        offline = in.readInt();
    }

    public static final Creator<Channel> CREATOR = new Creator<Channel>() {
        @Override
        public Channel createFromParcel(Parcel in) {
            return new Channel(in);
        }

        @Override
        public Channel[] newArray(int size) {
            return new Channel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(url);
        dest.writeString(port);
        dest.writeString(name);
        dest.writeInt(listener);
        dest.writeInt(unique);
        dest.writeInt(peak);
        dest.writeInt(offline);
    }
}
