package radio.com.radiomonitor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import radio.com.radiomonitor.model.StreamList;
import radio.com.radiomonitor.model.Stream;

/**
 * Created by triet.bui on 6/30/2016.
 */
public final class DataManager {

    public static List<Stream> getStreams() {
        List<Stream> streams = new ArrayList<>();

        Stream stream1 = new Stream();
        stream1.name = "EyeStream Icecast Mp3";

        StreamList streamList11 = new StreamList();
        streamList11.url = "http://nr6....";
        streamList11.port = "9316";
        streamList11.name = stream1.name;
        streamList11.listener = 42;
        streamList11.unique = 36;
        streamList11.peak = 2;
        streamList11.offline = 1;

        StreamList streamList12 = new StreamList();
        streamList12.url = "http://nr6....";
        streamList12.port = "9316";
        streamList12.name = stream1.name;
        streamList12.listener = 42;
        streamList12.unique = 36;
        streamList12.peak = 2;
        streamList12.offline = 1;

        stream1.list = Arrays.asList(streamList11, streamList12);
        streams.add(stream1);

        Stream stream2 = new Stream();
        stream1.name = "EyeStream SH1 Mp3";

        StreamList streamList21 = new StreamList();
        streamList21.url = "http://nr2....";
        streamList21.port = "123";
        streamList21.name = stream2.name;
        streamList21.listener = 42;
        streamList21.unique = 36;
        streamList21.peak = 2;
        streamList21.offline = 1;

        stream2.list = Arrays.asList(streamList21);
        streams.add(stream2);

        Stream stream3 = new Stream();
        stream1.name = "EyeStream Icecast Mp3";

        StreamList streamList31 = new StreamList();
        streamList11.url = "http://nr6....";
        streamList11.port = "9316";
        streamList11.name = stream1.name;
        streamList11.listener = 42;
        streamList11.unique = 36;
        streamList11.peak = 2;
        streamList11.offline = 1;

        StreamList streamList42 = new StreamList();
        streamList12.url = "http://nr6....";
        streamList12.port = "9316";
        streamList12.name = stream1.name;
        streamList12.listener = 42;
        streamList12.unique = 36;
        streamList12.peak = 2;
        streamList12.offline = 1;

        return streams;
    }

}
