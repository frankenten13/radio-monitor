package radio.com.radiomonitor;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ExpandableListView;

import java.util.List;

import radio.com.radiomonitor.model.Stream;

public class MainActivity extends AppCompatActivity {
    private ExpandableListView mEpNavigation;
    private List<Stream> mLvGroupTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mEpNavigation = (ExpandableListView)findViewById(R.id.expand_list);
        mLvGroupTitle = DataManager.getStreams();
        //CustomExpandAdapter mNavigationAdapter = new CustomExpandAdapter(this, mLvGroupTitle, mLvChildTitle);
        //mEpNavigation.setAdapter(mNavigationAdapter);


    }
}
