package radio.com.radiomonitor.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;

import radio.com.radiomonitor.R;
import radio.com.radiomonitor.model.Stream;

public class CustomExpandAdapter extends BaseExpandableListAdapter {

    private List<Stream> parentRecord;
    private LayoutInflater inflater = null;
    private Activity mContext;

    public CustomExpandAdapter(Activity context, List<Stream> parentList, HashMap<String, List<String>> childList) {
        this.parentRecord = parentList;
        mContext = context;
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public String getChild(int groupPosition, int childPosition) {
            return this.parentRecord.get(((Stream) getGroup(groupPosition)).getName()).get(childPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        String childConfig = getChild(groupPosition, childPosition);

        ViewHolder holder;
        try {
            if (convertView == null) {
                holder = new ViewHolder();

                convertView = inflater.inflate(R.layout.item_stream, null);
                holder.mIvStatus = (ImageView) convertView.findViewById(R.id.imageView_status);
                holder.childTitle = (TextView) convertView.findViewById(R.id.textView_title);
                holder.childLink = (TextView) convertView.findViewById(R.id.textView_link);
                holder.childListenner = (TextView) convertView.findViewById(R.id.textView_count);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            holder.childTitle.setText(childConfig);

        } catch (Exception e) {
        }
        return convertView;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        Stream parentSampleTo = parentRecord.get(groupPosition);

        ViewHolder holder;
        try {
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.item_streamlist, null);
                holder = new ViewHolder();

                holder.parentTitle = (TextView) convertView.findViewById(R.id.textView_title);
                /*if(groupPosition == 0){
                    holder.parentIcon_arrow.setVisibility(View.GONE);
                }else{
                    holder.parentIcon_arrow.setVisibility(View.VISIBLE);
                }*/

                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            holder.parentTitle.setText(parentSampleTo.getName());
            /*
            holder.parentIcon.setBackgroundResource(parentSampleTo.getIcon());
            // check if GroupView is expanded and set imageview for expand/collapse-action
            if(isExpanded){
                holder.parentIcon_arrow.setBackgroundResource(R.drawable.ic_show_more);
                holder.parentIcon.setSelected(true);
            }
            else{
                holder.parentIcon_arrow.setBackgroundResource(R.drawable.ic_show_more_left);
                holder.parentIcon.setSelected(false);
            }*/

        } catch (Exception e) {
        }
        return convertView;
    }

    public static class ViewHolder {

        private TextView childTitle;
        private TextView childLink;
        private TextView childListenner;
        private ImageView mIvStatus;
        // Content
        private TextView parentTitle;

    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this.childRecord.get(((Stream) getGroup(groupPosition)).getTitle()).size();
    }

    @Override
    public Stream getGroup(int groupPosition) {
        return this.parentRecord.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this.parentRecord.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

}
